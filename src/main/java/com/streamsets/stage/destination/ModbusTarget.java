package com.streamsets.stage.destination;

import com.streamsets.pipeline.api.Batch;
import com.streamsets.pipeline.api.Field;
import com.streamsets.pipeline.api.Record;
import com.streamsets.pipeline.api.StageException;
import com.streamsets.pipeline.api.base.BaseTarget;
import com.streamsets.pipeline.api.base.OnRecordErrorException;
import com.streamsets.pipeline.api.impl.Utils;
import com.streamsets.stage.lib.Errors;
import com.streamsets.stage.lib.Groups;
import com.streamsets.stage.lib.ModbusConstants;
import de.re.easymodbus.exceptions.ModbusException;
import de.re.easymodbus.modbusclient.ModbusClient;

import java.io.IOException;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Iterator;
import java.util.List;

public abstract class ModbusTarget extends BaseTarget {
    // private ELEval nameEval;
    // private ELVars nameVars;
    private ModbusClient modbusClient;
    @Override
    protected List<ConfigIssue> init() {
// Validate configuration values and open any required resources.
        List<ConfigIssue> issues = super.init();
        //  nameEval = getContext().createELEval("ipAddress");
        // nameVars = getContext().createELVars();
        modbusClient = new ModbusClient();
        if(!getIPAddress().startsWith("$")) {
            try {
                modbusClient.setUnitIdentifier((byte) getSlaveId());
                modbusClient.setUDPFlag(getIsUdp());
                modbusClient.setConnectionTimeout(getTimeout());
                modbusClient.Connect(getIPAddress(), getPort());

            } catch (IOException e) {
                issues.add(getContext().createConfigIssue(Groups.COMMUNICATION.name(), "ipAddress", Errors.ERROR_454, "init error"));
            }
        }
        return issues;
    }
    @Override
    public void destroy() {
        try {
            modbusClient.Disconnect();
        } catch (IOException e) {
            throw new StageException(Errors.ERROR_503);
        }
        super.destroy();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void write(Batch batch) throws StageException {
        Iterator<Record> batchIterator = batch.getRecords();
        while (batchIterator.hasNext()) {
            Record record = batchIterator.next();
            try {
                write(record);
            } catch (Exception e) {
                switch (getContext().getOnErrorRecord()) {
                    case DISCARD:
                        break;
                    case TO_ERROR:
                        getContext().toError(record, Errors.ERROR_001, e.toString());
                        break;
                    case STOP_PIPELINE:
                        throw new StageException(Errors.ERROR_001, e.toString());
                    default:
                        throw new IllegalStateException(
                                Utils.format("Unknown OnError value '{}'", getContext().getOnErrorRecord(), e)
                        );
                }
            }
        }
    }
    /**
     * Writes a single record to the destination.
     *
     * @param record the record to write to the destination.
     * @throws OnRecordErrorException when a record cannot be written.
     */
    private void write(Record record) throws StageException {
        if(getIPAddress().startsWith("$")) {
            String ipAddress = record.get("ipAddress").getValueAsString();
            try {
                modbusClient.setUnitIdentifier((byte) getSlaveId());
                modbusClient.setUDPFlag(getIsUdp());
                modbusClient.setConnectionTimeout(getTimeout());
                modbusClient.Connect(ipAddress, getPort());
                sendRecord(record.get("/address").getValueAsInteger(), record.get("/value"), record.get("/registerType").getValueAsString(), record.get("/dataType").getValueAsString());
                modbusClient.Disconnect();
            } catch (IOException e) {
                throw new OnRecordErrorException(Errors.ERROR_001);
            }
        }
        else{
            sendRecord(record.get("/address").getValueAsInteger(), record.get("/value"), record.get("/registerType").getValueAsString(), record.get("/dataType").getValueAsString());
        }

    }
    private void sendRecord(int address, Field value, String registerType, String dataType){
        try {
        switch (registerType) {
            case ModbusConstants.COIL_TYPE:
                modbusClient.WriteSingleCoil(address, value.getValueAsBoolean());
                break;
            case ModbusConstants.HOLDING_REGISTER_TYPE:
                int [] writeBytes;
                switch (dataType){
                    case ModbusConstants.FLOAT_CASE:
                        int inputValue = Float.floatToIntBits(value.getValueAsFloat());
                        writeBytes = new int[2];
                        writeBytes[1] = inputValue >>> 16;
                        writeBytes[0] = inputValue << 16;
                        modbusClient.WriteMultipleRegisters(address, writeBytes);
                        break;
                    case ModbusConstants.DWORD_CASE:
                        writeBytes = new int[2];
                        writeBytes[1] = (value.getValueAsInteger() << 16) >>> 16;
                        writeBytes[0] = value.getValueAsInteger() >>> 16;
                        modbusClient.WriteMultipleRegisters(address, writeBytes);
                        break;
                    case ModbusConstants.LONG_CASE:
                    case ModbusConstants.QWORD_CASE:
                        writeBytes = new int[4];
                        writeBytes[3] = (int) (value.getValueAsLong() << 48 >>> 48);
                        writeBytes[2] = (int) (value.getValueAsLong() << 32 >>> 48);
                        writeBytes[1] = (int) (value.getValueAsLong() << 16 >>> 48);
                        writeBytes[0] = (int) (value.getValueAsLong() >>> 48);
                        modbusClient.WriteMultipleRegisters(address, writeBytes);
                        break;
                    case ModbusConstants.DOUBLE_CASE:
                        writeBytes = new int[4];
                        long doubleBit = Double.doubleToLongBits(value.getValueAsDouble());
                        writeBytes[3] = (int)(doubleBit >>> 48);
                        writeBytes[2] = (int)(doubleBit << 16);
                        writeBytes[1] = (int)(doubleBit << 32);
                        writeBytes[0] = (int)(doubleBit << 48);
                        modbusClient.WriteMultipleRegisters(address, writeBytes);
                        break;
                    default:
                        writeBytes = new int[1];
                        writeBytes[0] = value.getValueAsShort();
                        modbusClient.WriteMultipleRegisters(address, writeBytes);
                }
               break;
            default:
                throw new OnRecordErrorException(Errors.ERROR_201);
            }
        } catch (UnknownHostException e) {
            throw new OnRecordErrorException(Errors.ERROR_454);
        } catch (SocketException e) {
            throw new OnRecordErrorException(Errors.ERROR_455);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ModbusException e) {
            throw new OnRecordErrorException(Errors.ERROR_200);
        }
    }

    public abstract String getIPAddress();

    public abstract int getPort();
    public abstract int getTimeout();
    public abstract boolean getIsUdp();
    public abstract int getSlaveId();
}