package com.streamsets.stage.destination;

import com.streamsets.pipeline.api.*;
import com.streamsets.stage.lib.Groups;
import com.streamsets.stage.lib.Menus.NetworkType;
import com.streamsets.stage.lib.Menus.NetworkTypeChooserValues;
import com.streamsets.stage.lib.ModbusConstants;

@StageDef(
        version = 1,
        label = ModbusConstants.DEST_STAGE_LABEL,
        description = ModbusConstants.STAGE_DESC,
        icon = ModbusConstants.STAGE_ICON,
        execution = ExecutionMode.STANDALONE,
        recordsByRef = true,
        onlineHelpRefUrl = ModbusConstants.STAGE_HELP_URL
)
@ConfigGroups(value = Groups.class)

@GenerateResourceBundle
public class ModbusDTarget extends ModbusTarget{
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.MODEL,
            defaultValue = ModbusConstants.ETHERNET_UDP_TYPE,
            label = ModbusConstants.UDP_FLAG_LABEL,
            description = ModbusConstants.UDP_FLAG_DESC,
            triggeredByValue = ModbusConstants.NETTYPE_ETH_OPTION,
            displayPosition = 10,
            group = ModbusConstants.COMM_GROUP
    )
    @ValueChooserModel(NetworkTypeChooserValues.class)
    public NetworkType isUdp;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.NUMBER,
            defaultValue = ModbusConstants.TIMEOUT_VALUE,
            label = ModbusConstants.TIMEOUT_LABEL,
            description = ModbusConstants.TIMEOUT_DESC,
            min=0,
            max=600000,
            displayPosition = 10,
            group = ModbusConstants.COMM_GROUP
    )
    public int timeout;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.STRING,
            defaultValue = ModbusConstants.IP_VALUE,
            label = ModbusConstants.IP_ADDR_LABEL,
            triggeredByValue = ModbusConstants.NETTYPE_ETH_OPTION,
            displayPosition = 10,
            group = ModbusConstants.COMM_GROUP,
            description = ModbusConstants.IP_ADDR_DESC
    )
    public String ipAddress;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.NUMBER,
            defaultValue = ModbusConstants.PORT_VALUE,
            label = ModbusConstants.PORT_LABEL,
            triggeredByValue = ModbusConstants.NETTYPE_ETH_OPTION,
            displayPosition = 10,
            group = ModbusConstants.COMM_GROUP,
            description = ModbusConstants.PORT_DESC
    )
    public int port;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.NUMBER,
            defaultValue = "0",
            label = ModbusConstants.COM_ID_LABEL,
            description = ModbusConstants.COM_ID_DESC,
            min=0,
            max=255,
            displayPosition = 10,
            group = ModbusConstants.COMM_GROUP
    )
    public int slaveId;

    /** {@inheritDoc} */
    @Override
    public String getIPAddress() {return ipAddress;}
    @Override
    public int getPort() {return port;}
    @Override
    public int getTimeout() {return timeout;}
    @Override
    public boolean getIsUdp() {
        boolean result;
        result = isUdp.name().equals(ModbusConstants.ETHERNET_UDP_TYPE);
        return result;
    }
    @Override
    public int getSlaveId() {return slaveId;}

}
