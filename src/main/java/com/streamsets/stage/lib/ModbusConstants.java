package com.streamsets.stage.lib;

public final class ModbusConstants {
    //////////////////////
    //Descriptions
    //////////////////////
    public static final String IP_ADDR_DESC = "Set Modbus server IP addrress (Default is 'localhost')";
    public static final String PREFIX_NAME_DESC = "Prefix string for each tag address. ex) myModbus->  tag data will be Mymodbus_HOLDINGREGISTER::1, prefix should be unique in order to duplicate with other pipeline prefix.";
    public static final String PORT_DESC = "Specify input modbus port for modbus can use of. Default value is 502";
    public static final String SCANRATE_DESC = "Select a Communication rate, note that set to '0' for send as soon as can";
    public static final String COMMTYPE_DESC = "Select a Communication type";
    public static final String STAGE_DESC = "Streamsets Modbus Data Adapter";
    public static final String TIMEOUT_DESC = "Timeout for each frame";
    public static final String INPUT_END_ADDR_DESC = "Input end address, Note that 32,64 Bits(DWORD/QWORD) will cut down to proper size. \nex) endaddress 1 to 10 with QWORD will assume 1 to 8 byte";
    public static final String INPUT_START_ADDR_DESC = "Input starting address should begin with 1 or 0 not 30001, or 30000";
    public static final String COMPORT_DESC = "Serial port for MODBUS communication,  the port string should include extensions with case sensitive.\n ex) /dev/ttyUSB0";
    public static final String TRANSFER_MODE_DESC = "If check, skip duplicated value for each data tags, substantial data reduce";
    public static final String IS_LITTLE_ENDIAN_DESC = "Swaps Hi-Lo bits  for WORD value not applied BOOLEAN type yet.";
    public static final String UDP_FLAG_DESC = "Check if the modbus connection us over UDP for ethernet";
    public static final String REGISTER_TYPE_DESC = "Choose register type, COIL:10000~19999, INPUT REGISTER:30000~39999, HOLDING REGISTER:40000~49999";
    public static final String COM_ID_DESC = "Input slave node ID for modbus serial(RS232/485)";
    public static final String DATA_TYPE_DESC = "Choose Data type that should be out";
    public static final String SCALE_DESC = "Input scale ratio by  1/10000. Default value is original value: '10000' ";
    public static final String BOOLEAN_OUTPUT_DESC = "If check, the boolean value is converted into 1/0 instead of True/False";
    public static final String DATA_MANAGE_SELECTION_DESC = "Select file format if you want read I/O Lists from local file instead of web page";
    public static final String ADDRESS_OFFSET_DESC = "If check, the first register address will be one(1), if not, begin with zero(0)";
    public static final String FILEPATH_DESC = "Input file path for the file. the filename should include extensions with case sensitive. ex)'/home/myHome/exam.csv'. note that address should 6 digits. ex) coil 1000 is 101000 File path can be retrieved system administrator";
    public static final String ADDR_WRITE_DESC = "Record key for Address that input";
    public static final String REG_TYPE_WRITE_DESC = "Record key for REGISTER TYPE that input";
    public static final String VALUE_WRITE_DESC = "Record key for value that input";
    /////////////////////
    //labels
    /////////////////////
    public static final String PREFIX_NAME_LABEL = "Prefix for each tag name";
    public static final String FILEPATH_LABEL = "File path and name";
    public static final String DATA_MANAGE_SELECTION_LABEL = "Read from local stored file";
    public static final String ADDRESS_OFFSET_LABEL = "Zero start address";
    public static final String IP_ADDR_LABEL = "IP Address";
    public static final String PORT_LABEL = "Modbus port";
    public static final String COMPORT_LABEL = "Serial port String";
    public static final String COMMTYPE_LABEL = "Communication Type";
    public static final String STAGE_LABEL = "Modbus Origin";
    public static final String DEST_STAGE_LABEL = "Modbus Destination";
    public static final String TIMEOUT_LABEL = "Timeout (Millisecs)";
    public static final String SCANRATE_LABEL = "Scan rate (Millisecs)";
    public static final String INPUT_START_ADDR_LABEL="Address";
    public static final String INPUT_END_ADDR_LABEL="End address";
    public static final String TRANSFER_MODE_LABEL = "Subscribe / polling";
    public static final String IS_LITTLE_ENDIAN_LABEL = "Swap Hi-Lo bits (Little Endian)";
    public static final String UDP_FLAG_LABEL = "Network Connection Type";
    public static final String REGISTER_TYPE_LABEL = "Register type";
    public static final String COM_ID_LABEL = "Modbus slave ID";
    public static final String SCALE_LABEL = "Scale";
    public static final String DATA_TYPE_LABEL = "Data Type";
    public static final String BOOLEAN_OUTPUT_LABEL = "Boolean output type";
    ////////////////
    //model values
    ///////////////
    ////////////////
    //model values.Comm type
    ///////////////
    public static final String ETHERNET_ETH_TYPE = "Ethernet (TCPIP/UDP)";
    public static final String SERIAL_TYPE = "Serial (RS232/RS485)";
    public static final String ETHERNET_UDP_TYPE = "UDP";
    public static final String ETHERNET_TCPIP_TYPE = "TCP/IP";
    ////////////////
    //model values BooleanOutput type
    ///////////////
    public static final String BOOLEAN_TYPE = "Original (True/false) output";
    public static final String INTEGER_TYPE = "Converted to Integer value (1/0) output";

    ////////////////////////////////
    //Model values.Register type
    ////////////////////////////////
    public static final String DISCRETE_INPUT = "Discrete Input (10000~19999)";
    public static final String COIL= "Coil (0~9999)";
    public static final String HOLDING_REGISTER = "Holding register(40000~49999)";
    public static final String INPUT_REGISTER = "Input register (30000~39999)";
    public static final String COIL_TYPE= "COIL";
    public static final String HOLDING_REGISTER_TYPE = "HOLDINGREGISTER";
    public static final String INPUT_REGISTER_TYPE = "INPUTREGISTER";
    public static final String DISCRETE_INPUT_TYPE = "DISCRETEINPUT";


    /////////////////////
    //Default Values
    /////////////////////
    public static final String FILEPATH_VALUE = "/home";
    public static final String ADDRESS_OFFSET_VALUE = "true";
    public static final String UDP_FLAG_VALUE = "true";
    public static final String TRANSFER_TYPE_VALUE = "true";
    public static final String IS_LITTLE_ENDIAN_VALUE = "false";
    public static final String SCANRATE_VALUE = "1000";
    public static final String TIMEOUT_VALUE = "1000";
    public static final String PORT_VALUE = "502";
    public static final String COMPORT_VALUE = "/dev/ttyS0";
    public static final String IP_VALUE = "localhost";
    ////////////////////////////////
    //Model values.dataType
    ////////////////////////////////
    public static final String BOOLEAN = "Boolean(1Bit)";
    public static final String DWORD = "Double Word(32Bit)";
    public static final String WORD = "Word (16Bit)";
    public static final String FLOAT = "Float (32Bit, IEEE-754 Method Read)";
    public static final String QWORD = "Quad Word (64Bit)";
    public static final String LONG = "Long (32Bit)";
    public static final String SHORT = "Short Integer (16Bit)";

    public static final String BOOLEAN_CASE = "boolean";
    public static final String DWORD_CASE = "dword";
    public static final String WORD_CASE = "word";
    public static final String FLOAT_CASE = "float";
    public static final String QWORD_CASE = "qword";
    public static final String DOUBLE_CASE = "double";
    public static final String LONG_CASE = "long";
    public static final String SIGNED_INTEGER_CASE = "short";
    /////////////////////
    //menugroups
    /////////////////////
    public static final String COMM_GROUP_LABEL = "Communication";
    public static final String ADDRESS_GROUP_LABEL = "Modbus address";
    public static final String COMM_GROUP = "COMMUNICATION";
    public static final String ADDRESS_GROUP = "ADDRESS";
    public static final String NETTYPE_ETH_OPTION = "ETHERNET";
    public static final String INTEGER_TYPE_OPTION = "INTEGERTYPE";
    public static final String NETTYPE_SER_OPTION = "SERIAL";
    public static final String KEPWARE_CSV_LABEL = "Csv format (Kepserver V6, Industirial Gateway V8 or later)";
    public static final String KEPWARE_CSV_OPTION = "KEPWARECSV";
    public static final String CUSTOM_CSV_OPTION = "CUSTOMCSV";
    public static final String CUSTOM_CSV_LABEL = "Custom CSV using AddressList format with 'RegisterType', 'startAddress and 'scale'";
    public static final String LIST_FROM_UI_OPTION = "LISTFROMUI";
    public static final String LIST_FROM_UI_LABEL = "Input my list using web page directly";

    ////////////////////////////////
    //External File Path
    ////////////////////////////////
    public static final String STAGE_ICON = "modbus.png";
    public static final String STAGE_HELP_URL = "";

    //////////////////////////
    //Constants
    //////////////////////////
    public static final int DEFAULT_KEPWARE_OFFSET = -1;
    public static final int KEPWARE_CSV_COLUMN = 17;
    public static final int BOOLEAN_MAX_READ_SIZE = 1000;
    public static final int WORD_MAX_READ_SIZE = 124;
    public static final double SCALE_FACTOR_BASE = 10000.0;
    public static final int READ_AHEAD_ADDRESS_QWORD = 4;
    public static final int READ_AHEAD_ADDRESS_DWORD = 2;
    public static final int READ_AHEAD_ADDRESS_WORD = 1;
    public static final int CO_LO_ADDRESS = 0;
    public static final int CO_HI_ADDRESS = 99999;
    public static final int DI_LO_ADDRESS = 100000;
    public static final int DI_HI_ADDRESS = 199999;
    public static final int IR_LO_ADDRESS = 300000;
    public static final int IR_HI_ADDRESS = 399999;
    public static final int HR_LO_ADDRESS = 400000;
    public static final int HR_HI_ADDRESS = 499999;
    public static final String ADDRESS_JSON_KEY = "address";
    public static final String REG_TYPE_JSON_KEY = "registerType";
    public static final String DEVICE_NAME_JSON_KEY = "deviceName";
    public static final String VALUE_JSON_KEY = "value";

}
