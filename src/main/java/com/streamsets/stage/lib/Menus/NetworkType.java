package com.streamsets.stage.lib.Menus;
import com.streamsets.pipeline.api.Label;
import com.streamsets.stage.lib.ModbusConstants;

public enum NetworkType implements Label {
    UDP(ModbusConstants.ETHERNET_UDP_TYPE),
    TCPIP(ModbusConstants.ETHERNET_TCPIP_TYPE);

    private final String label;

    NetworkType(String label) {
        this.label = label;
    }

    @Override
    public String getLabel() { return label; }
}