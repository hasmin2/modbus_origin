package com.streamsets.stage.lib.Menus;

import com.streamsets.pipeline.api.Label;
import com.streamsets.stage.lib.ModbusConstants;

public enum RegisterType implements Label {
    DISCRETEINPUT(ModbusConstants.DISCRETE_INPUT),
    COIL(ModbusConstants.COIL),
    HOLDINGREGISTER(ModbusConstants.HOLDING_REGISTER),
    INPUTREGISTER(ModbusConstants.INPUT_REGISTER);
    private final String label;
    RegisterType(String label) {
        this.label = label;
    }
    @Override
    public String getLabel() {
        return label;
    }
}
