package com.streamsets.stage.lib.Menus;
import com.streamsets.pipeline.api.Label;
import com.streamsets.stage.lib.ModbusConstants;

public enum CommType implements Label {
    ETHERNET(ModbusConstants.ETHERNET_ETH_TYPE),
    SERIAL(ModbusConstants.SERIAL_TYPE);

    private final String label;

    CommType(String label) {
        this.label = label;
    }

    @Override
    public String getLabel() { return label; }
}