package com.streamsets.stage.lib.Menus;

import com.streamsets.pipeline.api.Label;
import com.streamsets.stage.lib.ModbusConstants;

public enum BooleanOutPutType implements Label {
    INTEGERTYPE(ModbusConstants.INTEGER_TYPE),
    BOOLEANTYPE(ModbusConstants.BOOLEAN_TYPE);
    private final String label;
    BooleanOutPutType(String label) {
        this.label = label;
    }
    @Override
    public String getLabel() {
        return label;
    }
}
