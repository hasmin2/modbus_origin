package com.streamsets.stage.lib.Menus;

import com.streamsets.pipeline.api.Label;
import com.streamsets.stage.lib.ModbusConstants;


public enum ModbusDataType implements Label {
    BOOLEAN(ModbusConstants.BOOLEAN),
    WORD(ModbusConstants.WORD),
    DWORD(ModbusConstants.DWORD),
    QWORD(ModbusConstants.QWORD),
    SHORT(ModbusConstants.SHORT),
    LONG(ModbusConstants.LONG),
    FLOAT(ModbusConstants.FLOAT),
    ;
    private final String label;

    ModbusDataType(String label) { this.label = label; }

    @Override
    public String getLabel() {
        return label;
    }
}
