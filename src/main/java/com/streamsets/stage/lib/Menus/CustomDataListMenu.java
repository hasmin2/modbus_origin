package com.streamsets.stage.lib.Menus;

import com.streamsets.pipeline.api.ConfigDef;
import com.streamsets.pipeline.api.ValueChooserModel;
import com.streamsets.stage.lib.ModbusConstants;

public class CustomDataListMenu {
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.MODEL,
            label = ModbusConstants.REGISTER_TYPE_LABEL,
            defaultValue = "HOLDINGREGISTER",
            description = ModbusConstants.REGISTER_TYPE_DESC
    )
    @ValueChooserModel(RegisterTypeChooserValues.class)
    public RegisterType registerType;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.NUMBER,
            label = ModbusConstants.INPUT_START_ADDR_LABEL,
            defaultValue = "1",
            min = 0,
            max = 60000,
            description = ModbusConstants.INPUT_START_ADDR_DESC
    )
    public int address;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.MODEL,
            label = ModbusConstants.DATA_TYPE_LABEL,
            dependsOn = "registerType",
            triggeredByValue = {"HOLDINGREGISTER", "INPUTREGISTER"},
            defaultValue = "BOOLEAN",
            description = ModbusConstants.DATA_TYPE_DESC
    )
    @ValueChooserModel(ModbusDataTypeChooserValues.class)
    public ModbusDataType modbusDataType;
    @ConfigDef(
        required = true,
        type = ConfigDef.Type.NUMBER,
        label = ModbusConstants.SCALE_LABEL,
        defaultValue = "10000",
        min = 0,
        max = 1000000,
        dependsOn = "modbusDataType",
        triggeredByValue = {"WORD", "DWORD","QWORD","FLOAT","LONG", "SHORT"},
        description = ModbusConstants.SCALE_DESC
    )
    public double scale;
}