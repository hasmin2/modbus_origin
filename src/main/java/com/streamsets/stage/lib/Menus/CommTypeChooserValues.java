package com.streamsets.stage.lib.Menus;
import com.streamsets.pipeline.api.base.BaseEnumChooserValues;

public class CommTypeChooserValues extends BaseEnumChooserValues<CommType> {
    public CommTypeChooserValues() {
        super(CommType.class);
    }
}