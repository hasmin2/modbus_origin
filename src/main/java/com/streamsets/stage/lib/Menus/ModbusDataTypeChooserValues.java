package com.streamsets.stage.lib.Menus;

import com.streamsets.pipeline.api.base.BaseEnumChooserValues;

public class ModbusDataTypeChooserValues extends BaseEnumChooserValues<ModbusDataType> {
    public ModbusDataTypeChooserValues() {
        super(ModbusDataType.class);
    }
}
