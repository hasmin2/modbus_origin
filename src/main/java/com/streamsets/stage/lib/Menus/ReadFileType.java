package com.streamsets.stage.lib.Menus;

import com.streamsets.pipeline.api.Label;
import com.streamsets.stage.lib.ModbusConstants;

public enum ReadFileType implements Label {
    KEPWARECSV(ModbusConstants.KEPWARE_CSV_LABEL),
    //CUSTOMCSV(ModbusConstants.CUSTOM_CSV_LABEL),
    LISTFROMUI(ModbusConstants.LIST_FROM_UI_LABEL);
    //BUILT("More implemets if required");

    private final String label;

    ReadFileType(String label) {
        this.label = label;
    }

    @Override
    public String getLabel() {
        return label;
    }
}
