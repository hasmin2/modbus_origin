package com.streamsets.stage.lib.Menus;
import com.streamsets.pipeline.api.base.BaseEnumChooserValues;

public class RegisterTypeChooserValues extends BaseEnumChooserValues<RegisterType> {
    public RegisterTypeChooserValues() {
        super(RegisterType.class);
    }
}
