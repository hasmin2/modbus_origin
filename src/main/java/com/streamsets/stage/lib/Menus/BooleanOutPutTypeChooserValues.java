package com.streamsets.stage.lib.Menus;

import com.streamsets.pipeline.api.base.BaseEnumChooserValues;

public class BooleanOutPutTypeChooserValues extends BaseEnumChooserValues<BooleanOutPutType> {
    public BooleanOutPutTypeChooserValues() {
        super(BooleanOutPutType.class);
    }
}
