package com.streamsets.stage.lib;

import com.streamsets.pipeline.api.ErrorCode;
import com.streamsets.pipeline.api.GenerateResourceBundle;

@GenerateResourceBundle
public enum Errors implements ErrorCode {

    ERROR_001("Initialize failure. Detail is : {}"),
    ERROR_002("CSV file cannot be found as named in 'Device Name' Please verify the device name and filename, or file is located correctly"),
    ERROR_003("CSV file cannot be read while trying, check permission or file is corrupted.  {}"),
    ERROR_004("CSV encoding is incorrect check CSV encoding is EUC-KR or UTF-8.  {}"),
    ERROR_005("CSV file column is not compatible with designated. detail cause is : {}" ),
    ERROR_006("CSV address range cannot be recongnized. check address range is in proper range.  {}"),
    ERROR_007("CSV Data type cannot be recongnized. check Data type is correct 'WORD','DWORD' or etc.  {}"),
    ERROR_101("Set DWORD/QWORD address index is out of range that defined in 'Modbus address' tab {}"),
    ERROR_200("Check Address should fit in Integer : {}"),
    ERROR_201("Verify Register type : {}"),
    ERROR_202("Verify Data type should either 'boolean, signed integer,unsigned integer, word, dword, qword, float, double': {}"),
    ERROR_455("Ethernet Connection has established, However the TCP cannot get send Message.  {}"),
    ERROR_454("Ethernet Connection Has failed. Check Network cable is connected properly or can ping.  {}"),
    ERROR_456("Ethernet reconnection has failed. try to restart pipeline.  {}"),
    ERROR_465("Serial Connection has established, However the Serial port that listening cannot get send Message.  {}"),
    ERROR_464("Serial Port Connection Has failed. Check serial port is opened or ping command reachable{}"),
    ERROR_500("System Type cannot verified, for building address binary check System Type. Some model is not implemented!  {}"),
    ERROR_501("System Type cannot verified for building command binary, check System Type. Some model is not implemented!  {}"),
    ERROR_503("Modbus failed to disconnect, please make sure modbus is disconnected or port is already closed!  {}"),
    ERROR_504("System Type cannot verified for building CPU Location, check System Type. Some model is not implemented!  {}"),
    ERROR_505("Register Tag cannot verified during building the result address{}");
    private final String msg;

    Errors(String msg) {
        this.msg = msg;
    }

    /** {@inheritDoc} */
    @Override
    public String getCode() {
        return name();
    }

    /** {@inheritDoc} */
    @Override
    public String getMessage() {
        return msg;
    }
}
