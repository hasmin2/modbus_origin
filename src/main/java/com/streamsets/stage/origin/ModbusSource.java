package com.streamsets.stage.origin;
import com.streamsets.pipeline.api.BatchMaker;
import com.streamsets.pipeline.api.Field;
import com.streamsets.pipeline.api.Record;
import com.streamsets.pipeline.api.StageException;
import com.streamsets.pipeline.api.base.BaseSource;
import com.streamsets.stage.lib.Errors;
import com.streamsets.stage.lib.Groups;
import com.streamsets.stage.lib.Menus.BooleanOutPutType;
import com.streamsets.stage.lib.Menus.CustomDataListMenu;
import com.streamsets.stage.lib.Menus.ReadFileType;
import com.streamsets.stage.lib.ModbusConstants;
import com.streamsets.stage.origin.Connector.IOListFileReader;
import com.streamsets.stage.origin.Connector.KepwareCSVConnector;
import com.streamsets.stage.origin.Connector.ModbusConnector;
import com.streamsets.stage.origin.util.AddressListGenerator;
import com.streamsets.stage.origin.util.AddressListMenu;
import de.re.easymodbus.modbusclient.ModbusClient;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import static java.lang.Thread.sleep;

/**
 * This source is an example and does not actually read from anywhere.
 * It does however, generate generate a simple record with one field.
 */
public abstract class ModbusSource extends BaseSource {

    /**
     * Gives access to the UI configuration of the stage provided by the {@link ModbusDSource} class.
     */
    public abstract String getIPAddress();
    public abstract int getPort();
    public abstract boolean getTransferMode();
    public abstract boolean getIsLittleEndian();
    public abstract boolean getIsUdp();
    public abstract ReadFileType getCustomDataListFromCsv();

    public abstract boolean getIsBeginWithOne();

    public abstract BooleanOutPutType getBooleanOutputType();
    public abstract int getSlaveId();
    public abstract List<CustomDataListMenu> getCustomDataListMenus();
    public abstract String getFilePathName();
    public abstract int getTimeout();
    public abstract int getScanRate();
    private static final Logger log = LoggerFactory.getLogger(ModbusSource.class);
    private final Map <String, Object> lastValueMap = new HashMap<>();
    private ModbusConnector modbusConnector;
    private List <AddressListMenu> addressListMenuList;
    private List <CustomDataListMenu> customDataListMenuList;
    private List <Field> pollingList ;
    private int csvReadOffset = 0;

    @Override
    protected List<ConfigIssue> init() {
        List<ConfigIssue> issues = super.init();
        modbusConnector = new ModbusConnector();
        try {
            if(getCustomDataListFromCsv().name().equals(ModbusConstants.KEPWARE_CSV_OPTION)) {
                IOListFileReader kepwareCsvConnector = new KepwareCSVConnector();
                kepwareCsvConnector.init(getFilePathName());
                customDataListMenuList = kepwareCsvConnector.fileRead();
                addressListMenuList= kepwareCsvConnector.getAddressList();
                csvReadOffset = ModbusConstants.DEFAULT_KEPWARE_OFFSET;
            }
            else if (getCustomDataListFromCsv().name().equals(ModbusConstants.LIST_FROM_UI_OPTION)) {
                customDataListMenuList = getCustomDataListMenus();
                AddressListGenerator addressListGenerator = new AddressListGenerator(getCustomDataListMenus());
                addressListMenuList= addressListGenerator.getAddressListMenuList();
            }
           modbusConnector.init(getIPAddress(), getPort(), getTimeout(), getSlaveId(), getIsUdp());
        }
        catch (StageException e) {
            issues.add(getContext().createConfigIssue(Groups.ADDRESS.name(), "ipAddress", Errors.ERROR_001, e.getLocalizedMessage()));
        }
        return issues;
    }

    /** {@inheritDoc} */
    @Override
    public void destroy() {
        try { modbusConnector.disConnect(); }
        catch (Exception e) {
            log.warn("Modbus disconnection has failed, check connection is already closed");
            log.warn(e.getMessage());
            
        }
        super.destroy();
    }

    /** {@inheritDoc} */
    @Override
    public String produce(String lastSourceOffset, int maxBatchSize, BatchMaker batchMaker) throws StageException {
        modbusConnector.init(getIPAddress(), getPort(), getTimeout(), getSlaveId(), getIsUdp());
        long nextSourceOffset = 0;
        if (lastSourceOffset != null) { nextSourceOffset = Long.parseLong(lastSourceOffset); }
        long startTime = System.currentTimeMillis();
        /*if (!getCustomDataListFromCsv().name().equals(ModbusConstants.LIST_FROM_UI_OPTION) && !getIsBeginWithOne()) {
            int addressOffset = ModbusConstants.DEFAULT_KEPWARE_OFFSET;
        }*/
        pollingList = new ArrayList<>();
        Map <String, Boolean> coilMap = new HashMap<>();
        Map <String, Boolean> DIMap = new HashMap<>();
        Map <String, Integer> HRMap = new HashMap<>();
        Map <String, Integer> IRMap = new HashMap<>();
        for (AddressListMenu item : addressListMenuList) {
            int quantity = item.endAddress - item.startAddress+1;
            //nextSourceOffset, batchMaker, ModbusConstants.DISCRETE_INPUT_TYPE)
            switch (item.registerType.getLabel()) {
                case ModbusConstants.COIL:
                    coilMap.putAll(modbusConnector.readCoilMessages(item.startAddress+csvReadOffset, quantity));
                    break;
                case ModbusConstants.DISCRETE_INPUT:
                    DIMap.putAll(modbusConnector.readDIMessages(item.startAddress+csvReadOffset, quantity));
                    break;
                case ModbusConstants.HOLDING_REGISTER:
                    HRMap.putAll(modbusConnector.readHRMessages(item.startAddress+csvReadOffset, quantity, getIsLittleEndian()));
                    break;
                case ModbusConstants.INPUT_REGISTER:
                    IRMap.putAll(modbusConnector.readIRMessages(item.startAddress+csvReadOffset, quantity, getIsLittleEndian()));
                    break;
                default:
                    log.info("Not recongnize message header received. Detail is:" + item.registerType);
            }
        }
        if(coilMap.size()>0) { nextSourceOffset += sendBoolRecord(coilMap, nextSourceOffset, batchMaker, ModbusConstants.COIL_TYPE); }
        if(DIMap.size()>0) { nextSourceOffset+= sendBoolRecord(DIMap, nextSourceOffset, batchMaker, ModbusConstants.DISCRETE_INPUT_TYPE);}
        if(HRMap.size()>0) { nextSourceOffset+= sendNotBoolRecords(HRMap, nextSourceOffset, batchMaker, ModbusConstants.HOLDING_REGISTER_TYPE);}
        if(IRMap.size()>0) { nextSourceOffset+= sendNotBoolRecords(IRMap, nextSourceOffset, batchMaker, ModbusConstants.INPUT_REGISTER_TYPE); }

        long endTime = System.currentTimeMillis();
        long interval = getInterval(startTime, endTime);
        if(!getTransferMode()){
            sendRecordList(batchMaker, nextSourceOffset);
            nextSourceOffset++;
        }
        try {
            if(interval>getTimeout()-100) {
                modbusConnector.disConnect();
            }
            sleep(interval);
        }
        catch (InterruptedException e) { throw new StageException(Errors.ERROR_001);}
        return String.valueOf(nextSourceOffset);
    }
    private long getInterval (long startTime, long endTime){
        long elaspedTime = (endTime - startTime);
        long interval = getScanRate() - elaspedTime;
        return interval > 0 ? interval : 0;
    }

    private long sendBoolRecord(Map<String, Boolean> dataMap, long nextSourceOffset, BatchMaker batchMaker, String registerType){
        for (CustomDataListMenu item : customDataListMenuList) {
            if(item.registerType.name().equals(registerType)) {
                String baseAddressString = registerType + "_" + item.address;
                if(getTransferMode()){
                    if (isNotDuplicateValue(baseAddressString, dataMap.get(baseAddressString))) {
                        composeBoolRecord(baseAddressString, registerType, dataMap.get(baseAddressString), batchMaker, nextSourceOffset);
                        ++nextSourceOffset;
                    }
                }
                else {
                    addPollingList(baseAddressString, dataMap.get(baseAddressString));
                }
            }
        }
        return nextSourceOffset;
    }

    private void composeBoolRecord(String address, String registerType , boolean value, BatchMaker batchMaker, long nextSourceOffset){
        if (getBooleanOutputType().name().equals(ModbusConstants.INTEGER_TYPE_OPTION)) {
            sendRecord(registerType +"_"+ address, value ? 1:0, batchMaker, nextSourceOffset);
        }
        else { addPollingList(registerType +"_"+ address, value); }
    }
    private String [] getAddressArray(String registerType, int address, int length){
        String [] result = new String [length];
        for (int i=0;i<length;i++){
            result[i] = registerType+"_"+address;
            address++;
        }
        return result;
    }

    private long sendNotBoolRecords(Map<String, Integer> dataMap, long nextSourceOffset, BatchMaker batchMaker, String registerType) throws StageException {
        for (CustomDataListMenu item : customDataListMenuList) {
            if(item.registerType.name().equals(registerType)){
                int[] wordArray;
                double scaleFactor;
                String baseAddressString = registerType+"_"+item.address;
                //Qword if....
                if (ModbusConstants.QWORD.equals(item.modbusDataType.getLabel())) {
                    scaleFactor = item.scale;
                    long resultValue;
                    try {
                        String [] addrStr = getAddressArray(registerType, item.address, 4);
                        wordArray = new int [4];
                        for (int i=0;i<wordArray.length;i++){
                            wordArray[i] = dataMap.get(addrStr[wordArray.length-1-i]);
                        }
                        resultValue = ModbusClient.ConvertRegistersToLong(wordArray, ModbusClient.RegisterOrder.HighLow);
                    } catch (IndexOutOfBoundsException ie) {
                        throw new StageException(Errors.ERROR_101, ie.getMessage());
                    }
                    if(getTransferMode()) {
                        if (isNotDuplicateValue(baseAddressString, resultValue)) {
                            sendRecord(baseAddressString, resultValue * scaleFactor / ModbusConstants.SCALE_FACTOR_BASE, batchMaker, nextSourceOffset);
                            ++nextSourceOffset;
                        }
                    }
                    else { addPollingList(baseAddressString, resultValue * scaleFactor / ModbusConstants.SCALE_FACTOR_BASE); }
                }//Dword if....
                else if (ModbusConstants.DWORD.equals(item.modbusDataType.getLabel()) || ModbusConstants.LONG.equals(item.modbusDataType.getLabel())) {
                    scaleFactor = item.scale;
                    long resultValue;
                    try {
                        String [] addrStr = getAddressArray(registerType, item.address, 2);
                        wordArray = new int [2];
                        for (int i=0;i<wordArray.length;i++){
                            wordArray[i] = dataMap.get(addrStr[wordArray.length-1-i]);
                        }
                        if (ModbusConstants.DWORD.equals(item.modbusDataType.getLabel())) {
                            resultValue = ModbusClient.ConvertRegistersToDouble(wordArray, ModbusClient.RegisterOrder.HighLow) & 0xFFFFFFFFL;
                        } else {
                            resultValue = ModbusClient.ConvertRegistersToDouble(wordArray, ModbusClient.RegisterOrder.HighLow);
                        }
                    }
                    catch (IndexOutOfBoundsException ie) {
                        throw new StageException(Errors.ERROR_101, ie.getMessage());
                    }
                    if(getTransferMode()) {
                        if (isNotDuplicateValue(baseAddressString, resultValue)) {
                            sendRecord(baseAddressString, resultValue * scaleFactor / ModbusConstants.SCALE_FACTOR_BASE, batchMaker, nextSourceOffset);
                            ++nextSourceOffset;
                        }
                    }
                    else {
                        addPollingList(baseAddressString, resultValue * scaleFactor / ModbusConstants.SCALE_FACTOR_BASE);
                    }
                }
                else if (ModbusConstants.FLOAT.equals(item.modbusDataType.getLabel())) {
                    String [] addrStr = getAddressArray(registerType, item.address, 2);
                    wordArray = new int [2];
                    for (int i=0;i<wordArray.length;i++){
                        wordArray[i] = dataMap.get(addrStr[wordArray.length-1-i]);
                    }
                    scaleFactor = item.scale;
                    float resultValue;
                    try {
                        resultValue = ModbusClient.ConvertRegistersToFloat(wordArray, ModbusClient.RegisterOrder.HighLow);
                    } catch (IndexOutOfBoundsException ie) {
                        throw new StageException(Errors.ERROR_101, ie.getMessage());
                    }
                    if(getTransferMode()) {
                        if (isNotDuplicateValue(baseAddressString, resultValue)) {
                            sendRecord(baseAddressString, resultValue * scaleFactor / ModbusConstants.SCALE_FACTOR_BASE, batchMaker, nextSourceOffset);
                            ++nextSourceOffset;
                        }
                    }
                    else {
                        addPollingList(baseAddressString, resultValue * scaleFactor / ModbusConstants.SCALE_FACTOR_BASE);
                    }
                }
                else if (ModbusConstants.BOOLEAN.equals(item.modbusDataType.getLabel())) {
                    String resultBoolString = StringUtils.leftPad(Integer.toBinaryString((int) (dataMap.get(baseAddressString) & 0xFFFFL)), 16, '0');
                    for (int j = 0; j < 16; j++) {
                        String addressBoolString = registerType + "_" + item.address + "_" + j;
                        boolean currentBoolValue = Integer.parseInt(resultBoolString.substring(15 - j, 15 - j + 1)) == 1;
                        if (getTransferMode()) {
                            if (isNotDuplicateValue(baseAddressString, currentBoolValue)) {
                                composeBoolRecord(addressBoolString, registerType, currentBoolValue, batchMaker, nextSourceOffset);
                                ++nextSourceOffset;
                            }
                        } else {
                            addPollingList(addressBoolString , currentBoolValue);
                        }
                    }
                }
                else {
                    int resultValue = 0;
                    if (ModbusConstants.WORD.equals(item.modbusDataType.getLabel())) {
                        resultValue = (int) (dataMap.get(baseAddressString) & 0XFFFFL);
                    }
                    if (ModbusConstants.SHORT.equals(item.modbusDataType.getLabel())) {
                        resultValue = dataMap.get(baseAddressString);
                    }
                    scaleFactor = item.scale;
                    if(getTransferMode()) {
                        if (isNotDuplicateValue(baseAddressString, resultValue)) {
                            sendRecord(registerType + "_" + item.address, resultValue * scaleFactor / ModbusConstants.SCALE_FACTOR_BASE, batchMaker, nextSourceOffset);
                            ++nextSourceOffset;
                        }
                    }
                    else {
                        addPollingList(registerType + "_" + item.address, resultValue * scaleFactor / ModbusConstants.SCALE_FACTOR_BASE);
                    }
                }
            }
        }
        return nextSourceOffset;
    }

    private <T> void sendRecord(String address, T value, @NotNull BatchMaker batchMaker, long nextSourceOffset){
        Record record = getContext().createRecord(String.valueOf(nextSourceOffset));
        Map <String, Field> fields = new HashMap<>();
        Field.Type type = getType(value);
        fields.put(ModbusConstants.ADDRESS_JSON_KEY, Field.create(address));
        fields.put(ModbusConstants.VALUE_JSON_KEY, Field.create(type, value));
        fields.put("timestamp", Field.createDate(new Date()));
        record.set(Field.create(fields));
        batchMaker.addRecord(record);
    }
    private void sendRecordList(@NotNull BatchMaker batchMaker, long nextSourceOffset){
        Record record = getContext().createRecord(String.valueOf(nextSourceOffset));
        record.set(Field.create(pollingList));
        batchMaker.addRecord(record);
    }

    private <T> void addPollingList(String address, T value){
        Map <String, Field> fields = new HashMap<>();
        Field.Type type = getType(value);
        fields.put(ModbusConstants.ADDRESS_JSON_KEY, Field.create(address));
        fields.put(ModbusConstants.VALUE_JSON_KEY, Field.create(type, value));
        pollingList.add(Field.create(fields));
    }

    private <T> Field.Type getType(T value) {
        Field.Type result;
        if(value instanceof Integer){ result = Field.Type.INTEGER; }
        else if (value instanceof Boolean){ result = Field.Type.BOOLEAN; }
        else if (value instanceof Long) {result = Field.Type.LONG; }
        else if (value instanceof Double){ result = Field.Type.DOUBLE; }
        else if (value instanceof Float){ result = Field.Type.DOUBLE; }
        else { result = Field.Type.STRING; }
        return result;
    }

    private <T> boolean isNotDuplicateValue(String addressString, T value){
        boolean returnResult;
        if(lastValueMap.containsKey(addressString)){
            if(lastValueMap.get(addressString).equals(value)){ returnResult = true;}
            else {
                lastValueMap.put(addressString, value);
                returnResult=false;
            }
        }
        else {
            lastValueMap.put(addressString, value);
            returnResult = false;
        }
        return !returnResult;
    }
}
