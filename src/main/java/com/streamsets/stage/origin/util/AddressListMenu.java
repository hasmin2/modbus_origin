package com.streamsets.stage.origin.util;

import com.streamsets.pipeline.api.ConfigDef;
import com.streamsets.pipeline.api.ValueChooserModel;
import com.streamsets.stage.lib.Menus.RegisterType;
import com.streamsets.stage.lib.Menus.RegisterTypeChooserValues;
import com.streamsets.stage.lib.ModbusConstants;

public class AddressListMenu {
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.MODEL,
            label = ModbusConstants.REGISTER_TYPE_LABEL,
            defaultValue = "COIL",
            description = ModbusConstants.REGISTER_TYPE_DESC
    )
    @ValueChooserModel(RegisterTypeChooserValues.class)
    public RegisterType registerType = RegisterType.COIL;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.NUMBER,
            label = ModbusConstants.INPUT_START_ADDR_LABEL,
            defaultValue = "1",
            min = 0,
            max = 60000,
            description = ModbusConstants.INPUT_START_ADDR_DESC
    )
    public int startAddress = 0;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.NUMBER,
            label = ModbusConstants.INPUT_END_ADDR_LABEL,
            defaultValue = "1",
            min = 0,
            max = 60000,
            description = ModbusConstants.INPUT_END_ADDR_DESC
    )
    public int endAddress = 0;
}
