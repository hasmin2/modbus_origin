package com.streamsets.stage.origin.util;

import com.streamsets.stage.lib.Menus.CustomDataListMenu;
import com.streamsets.stage.lib.Menus.ModbusDataType;
import com.streamsets.stage.lib.Menus.RegisterType;
import com.streamsets.stage.lib.ModbusConstants;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

public class AddressListGenerator {
    private final List<AddressListMenu> addressListMenuList;
    private final List<CustomDataListMenu> customDataListMenuList;

    public AddressListGenerator(List<CustomDataListMenu> customDataListMenuList){
        addressListMenuList = new ArrayList<>();
        this.customDataListMenuList = customDataListMenuList;
        generateAddressList();
    }
    public List<AddressListMenu> getAddressListMenuList(){return addressListMenuList; }
    private void generateAddressList(){
        TreeMap<Integer, ModbusDataType> coilList = new TreeMap<>();
        TreeMap<Integer, ModbusDataType> dIList = new TreeMap<>();
        TreeMap<Integer, ModbusDataType> iRList = new TreeMap<>();
        TreeMap<Integer, ModbusDataType> hRList = new TreeMap<>();
        for(CustomDataListMenu item : customDataListMenuList){
            switch (item.registerType.getLabel()) {
                case ModbusConstants.COIL:
                    coilList.put(item.address, item.modbusDataType);
                    break;
                case ModbusConstants.DISCRETE_INPUT:
                    dIList.put(item.address, item.modbusDataType);
                    break;
                case ModbusConstants.INPUT_REGISTER:
                    iRList.put(item.address, item.modbusDataType);
                    break;
                default:
                    hRList.put(item.address, item.modbusDataType);
                    break;
            }
        }
        if(coilList.size()>0) { makeAddressMenuList(generateAddressList(coilList), RegisterType.COIL);}
        if(dIList.size()>0) { makeAddressMenuList(generateAddressList(dIList), RegisterType.DISCRETEINPUT); }
        if(iRList.size()>0) { makeAddressMenuList(generateAddressList(iRList), RegisterType.INPUTREGISTER); }
        if(hRList.size()>0) { makeAddressMenuList(generateAddressList(hRList), RegisterType.HOLDINGREGISTER); }
    }
    private void makeAddressMenuList(List<Integer[]> inputResultList, RegisterType registerType){
        for (Integer[] item : inputResultList){ addToAddressMenuList(item[0], item[1], registerType);}
    }
    private List <Integer[]> generateAddressList(TreeMap<Integer, ModbusDataType> inputList){
        List <Integer[]> result = new ArrayList<>();
        int startAddress = inputList.firstKey();
        int endAddress = inputList.firstKey();
        for (Integer key : inputList.keySet()) {
            ModbusDataType dataType = inputList.get(key);
            if (dataType.name().equals(ModbusDataType.QWORD.name())) {
                if (endAddress + ModbusConstants.READ_AHEAD_ADDRESS_QWORD < key) {
                    result.add(new Integer[]{startAddress, endAddress});
                    startAddress = key;
                }
                endAddress = key + ModbusConstants.READ_AHEAD_ADDRESS_QWORD;
            } else if (dataType.name().equals(ModbusDataType.DWORD.name()) || dataType.name().equals(ModbusDataType.FLOAT.name()) || dataType.name().equals(ModbusDataType.LONG.name())) {
                if (endAddress + ModbusConstants.READ_AHEAD_ADDRESS_DWORD < key) {
                    result.add(new Integer[]{startAddress, endAddress});
                    startAddress = key;
                }
                endAddress = key + ModbusConstants.READ_AHEAD_ADDRESS_DWORD;
            }  else {
                if (endAddress + ModbusConstants.READ_AHEAD_ADDRESS_WORD < key) {
                    result.add(new Integer[]{startAddress, endAddress});
                    startAddress = key;
                }
                endAddress = key + ModbusConstants.READ_AHEAD_ADDRESS_WORD;
            }
        }
        result.add(new Integer[] {startAddress, endAddress});
        return result;
    }

    private void addToAddressMenuList(int loAddress, int hiAddress, RegisterType registerType){
        AddressListMenu addressList = new AddressListMenu();
        addressList.registerType = registerType;
        addressList.startAddress = loAddress;
        addressList.endAddress = hiAddress;
        addressListMenuList.add(addressList);
    }
}
