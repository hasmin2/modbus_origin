package com.streamsets.stage.origin.Connector;

import com.streamsets.pipeline.api.StageException;
import com.streamsets.stage.lib.Errors;
import com.streamsets.stage.lib.ModbusConstants;
import de.re.easymodbus.exceptions.ModbusException;
import de.re.easymodbus.modbusclient.ModbusClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static java.lang.Thread.sleep;

public class ModbusConnector {
    private static final Logger log = LoggerFactory.getLogger(ModbusConnector.class);
    private String ipPort;
    private ModbusClient modbusClient;
    public void init (String ip, int port, int connectionTimeout, int slaveId, boolean isUdp) throws StageException {
        this.ipPort = ip;
        modbusClient = new ModbusClient();
        try {
            modbusClient.setUnitIdentifier((byte) slaveId);
            modbusClient.setUDPFlag(isUdp);
            modbusClient.setConnectionTimeout(connectionTimeout);
            modbusClient.Connect(ip, port);
        }
        catch (IOException e) { throw new StageException(Errors.ERROR_454, e.getMessage());}
    }
    public void init (String port, int connectionTimeout, int slaveId) throws StageException {
        this.ipPort = port;
        modbusClient = new ModbusClient();
        modbusClient.setUnitIdentifier((byte) slaveId);
        try {
            if(!modbusClient.isConnected()) {
                modbusClient.setConnectionTimeout(connectionTimeout);
                modbusClient.Connect(ipPort);
            }
        }
        catch (Exception e) { throw new StageException(Errors.ERROR_464, e.getMessage());}
    }
    public Map<String, Boolean> readCoilMessages(int startAddress, int quantity) throws StageException {
        Map<String, Boolean> result = new HashMap<>();
        int curAddress = startAddress;
        try {
            int[] qnr = getQNR(quantity, false);
            boolean[] readResult;
            for (int i = 0; i < qnr[0]; i++) {
                readResult = modbusClient.ReadCoils(curAddress, ModbusConstants.BOOLEAN_MAX_READ_SIZE);
                result.putAll(addBoolResultMap(ModbusConstants.COIL_TYPE, curAddress, readResult));
                curAddress+= ModbusConstants.BOOLEAN_MAX_READ_SIZE;
            }
            readResult = modbusClient.ReadCoils(curAddress, qnr[1]);
            result.putAll(addBoolResultMap(ModbusConstants.COIL_TYPE, curAddress, readResult));
        }
        catch (ModbusException e) {
            log.info("Modbus read error, timeout or LAN cable disconnected");
            log.info(e.getMessage());
        }
        catch (IOException e) {
            log.info("Network Disconnect, retry to reconnect...");
            try {
                modbusClient.Disconnect();
                sleep(3000);
                modbusClient.Connect();
            } catch (IOException e1) {
                log.info("Network Disconnected. try to restart pipeline");
                log.info(e.getMessage());
                throw new StageException(Errors.ERROR_456, e1.getMessage());
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
        }
        return result;
    }
    public Map<String, Boolean> readDIMessages(int startAddress, int quantity) throws StageException {
        Map<String, Boolean> result = new HashMap<>();
        int curAddress = startAddress;
        try {
            int[] qnr = getQNR(quantity, false);
            boolean[] readResult;
            for (int i = 0; i < qnr[0]; i++) {
                readResult = modbusClient.ReadDiscreteInputs(curAddress, ModbusConstants.BOOLEAN_MAX_READ_SIZE);
                result.putAll(addBoolResultMap(ModbusConstants.DISCRETE_INPUT_TYPE, curAddress, readResult));
                curAddress += ModbusConstants.BOOLEAN_MAX_READ_SIZE;
            }
            readResult = modbusClient.ReadDiscreteInputs(startAddress, qnr[1]);
            result.putAll(addBoolResultMap(ModbusConstants.DISCRETE_INPUT_TYPE, curAddress, readResult));
        }
        catch (ModbusException e) {
            log.info("Modbus read error, timeout or LAN cable disconnected");
            log.info(e.getMessage());
        }
        catch (IOException e) {
            log.info("Network Disconnect, retry to reconnect...");
            try {
                modbusClient.Disconnect();
                sleep(3000);
                modbusClient.Connect();
            } catch (IOException e1) {
                log.info("Network Disconnected. try to restart pipeline");
                log.info(e.getMessage());
                throw new StageException(Errors.ERROR_456, e1.getMessage());
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
        }
        return result;
    }

    private Map <String, Boolean> addBoolResultMap (String registerType, int address, boolean [] itemMap){
        Map <String, Boolean> result = new HashMap<>();
        for (boolean item : itemMap) {
            result.put(registerType + "_" + address, item);
            address++;
        }
        return result;
    }
    private Map <String, Integer> addWordResultMap (String registerType, int address, int [] itemMap, boolean isLittleEndian){
        Map <String, Integer> result = new HashMap<>();
        for (int item : itemMap) {
            Integer resultNum;
            if(isLittleEndian) { resultNum = swap(item); }
            else { resultNum = item; }
            result.put(registerType+"_"+address, resultNum);
            address++;
        }
        return result;
    }

    public Map <String, Integer> readHRMessages(int startAddress, int quantity, boolean isLittleEndian) throws StageException {
        int curAddress = startAddress;
        Map <String, Integer> result = new HashMap<>();
        try {
            int [] qnr = getQNR(quantity, true);
            int [] readResult;
            for (int i=0;i < qnr[0];i++) {
                readResult = modbusClient.ReadHoldingRegisters(curAddress, ModbusConstants.WORD_MAX_READ_SIZE);
                result.putAll(addWordResultMap(ModbusConstants.HOLDING_REGISTER_TYPE, curAddress, readResult, isLittleEndian));
                curAddress+=ModbusConstants.WORD_MAX_READ_SIZE;
            }
            readResult = modbusClient.ReadHoldingRegisters(curAddress, qnr[1]);
            result.putAll(addWordResultMap(ModbusConstants.HOLDING_REGISTER_TYPE, curAddress, readResult, isLittleEndian));
        }
        catch (ModbusException e) {
            log.info("Modbus read error, timeout or LAN cable disconnected");
            log.info(e.getMessage());
        }
        catch (IOException e) {
            log.info("Network Disconnect, retry to reconnect...");
            try {
                modbusClient.Disconnect();
                sleep(3000);
                modbusClient.Connect();
            } catch (IOException e1) {
                log.info("Network Disconnected. try to restart pipeline");
                log.info(e.getMessage());
                throw new StageException(Errors.ERROR_456, e1.getMessage());
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
        }
        return result;
    }
    public Map <String, Integer> readIRMessages(int startAddress, int quantity, boolean isLittleEndian) throws StageException {
        int curAddress = startAddress;
        Map <String, Integer> result = new HashMap<>();
        try {
            int [] qnr = getQNR(quantity, true);
            int [] readResult;
            for (int i=0;i < qnr[0];i++) {
                readResult = modbusClient.ReadInputRegisters(curAddress, ModbusConstants.WORD_MAX_READ_SIZE);
                result.putAll(addWordResultMap(ModbusConstants.INPUT_REGISTER_TYPE, curAddress, readResult, isLittleEndian));
                curAddress+=ModbusConstants.WORD_MAX_READ_SIZE;
            }
            readResult = modbusClient.ReadInputRegisters(curAddress, qnr[1]);
            result.putAll(addWordResultMap(ModbusConstants.INPUT_REGISTER_TYPE, curAddress, readResult, isLittleEndian));
        }
        catch (ModbusException e) {
            log.info("Modbus read error, timeout or LAN cable disconnected");
            log.info(e.getMessage());
        }
        catch (IOException e) {
            log.info("Network Disconnect, retry to reconnect...");
            try {
                modbusClient.Disconnect();
                sleep(3000);
                modbusClient.Connect();
            } catch (IOException e1) {
                log.info("Network Disconnected. try to restart pipeline");
                log.info(e.getMessage());
                throw new StageException(Errors.ERROR_456, e1.getMessage());
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
        }
        return result;
    }
    private int [] getQNR(int quantity, boolean isWord){
        int [] result = new int [2];
        if(isWord){
            result[0] = quantity/ModbusConstants.WORD_MAX_READ_SIZE;
            result[1] = quantity%ModbusConstants.WORD_MAX_READ_SIZE;
        }
        else {
            result[0] = quantity/ModbusConstants.BOOLEAN_MAX_READ_SIZE;
            result[1] = quantity%ModbusConstants.BOOLEAN_MAX_READ_SIZE;
        }
        return result;
    }
    public void disConnect() throws StageException {
        try {
            modbusClient.Disconnect();
        } catch (IOException e) {
            e.printStackTrace();
            throw new StageException(Errors.ERROR_503, e.getMessage());
        }
    }
    private Integer swap(Integer x) { return (Integer)((x << 8) | ((x >> 8) & 0xff)); }
    private char swap(char x) { return (char)((x << 8) | ((x >> 8) & 0xff)); }
    private int swap(int x) { return (int)((swap((Integer)x) << 16) | swap((Integer)(x >> 16)) & 0xffff); }
    private long swap(long x) { return (long)(((long)swap((int)(x)) << 32) | ((long)swap((int)(x >> 32)) & 0xffffffffL)); }
    private float swap(float x) { return Float.intBitsToFloat(swap(Float.floatToRawIntBits(x))); }
    private double swap(double x) { return Double.longBitsToDouble(swap(Double.doubleToRawLongBits(x))); }
}
