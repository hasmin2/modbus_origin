package com.streamsets.stage.origin.Connector;

import com.opencsv.CSVReader;
import com.streamsets.pipeline.api.StageException;
import com.streamsets.stage.lib.Errors;
import com.streamsets.stage.lib.Menus.CustomDataListMenu;
import com.streamsets.stage.lib.Menus.ModbusDataType;
import com.streamsets.stage.lib.Menus.RegisterType;
import com.streamsets.stage.lib.ModbusConstants;
import com.streamsets.stage.origin.util.AddressListGenerator;
import com.streamsets.stage.origin.util.AddressListMenu;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class KepwareCSVConnector implements IOListFileReader{
    private CSVReader reader;
    private List<AddressListMenu> addressListMenuList;
    @Override
    public void init(String fileName) throws StageException {
        try {
            reader = new CSVReader(new InputStreamReader(new FileInputStream(fileName), "UTF-8"));
            addressListMenuList = new ArrayList<>();
            String [] columnLabel = reader.readNext();
            if(columnLabel.length != ModbusConstants.KEPWARE_CSV_COLUMN){
                throw new StageException(Errors.ERROR_005, "Expected column : " + ModbusConstants.KEPWARE_CSV_COLUMN + "Actual Column : " + String.valueOf(columnLabel.length));
            }

        }
        catch (UnsupportedEncodingException e) { throw new StageException(Errors.ERROR_004, e.getMessage()); }
        catch (FileNotFoundException e) { throw new StageException(Errors.ERROR_002, e.getMessage()); }
        catch (IOException e) { throw new StageException(Errors.ERROR_003, e.getMessage()); }
    }

    @Override
    public List<CustomDataListMenu> fileRead() throws StageException {
        List<CustomDataListMenu> resultList = new ArrayList<>();
        try {
            String[] columnValue;
            while ((columnValue = reader.readNext()) != null) {
                CustomDataListMenu customDataList = new CustomDataListMenu();
                int addressInt = getAddressInteger(columnValue[1]);
                if(addressInt < ModbusConstants.DI_HI_ADDRESS && addressInt >= ModbusConstants.DI_LO_ADDRESS){
                    customDataList.registerType = RegisterType.DISCRETEINPUT;
                    addressInt -= ModbusConstants.DI_LO_ADDRESS;
                }
                else if(addressInt < ModbusConstants.CO_HI_ADDRESS && addressInt >= ModbusConstants.CO_LO_ADDRESS){
                    customDataList.registerType = RegisterType.COIL;
                }
                else if(addressInt < ModbusConstants.HR_HI_ADDRESS && addressInt >= ModbusConstants.HR_LO_ADDRESS){
                    customDataList.registerType = RegisterType.HOLDINGREGISTER;
                    addressInt -= ModbusConstants.HR_LO_ADDRESS;
                }
                else if (addressInt < ModbusConstants.IR_HI_ADDRESS && addressInt >= ModbusConstants.IR_LO_ADDRESS){
                    customDataList.registerType = RegisterType.INPUTREGISTER;
                    addressInt -= ModbusConstants.IR_LO_ADDRESS;
                }
                else { if (addressInt != -1) { throw new StageException(Errors.ERROR_006); } }
                if(addressInt!=-1) {
                    customDataList.address = addressInt;
                    if (columnValue[10].equals("") || columnValue[8].equals("")) { customDataList.scale = ModbusConstants.SCALE_FACTOR_BASE; }
                    else { customDataList.scale = Integer.parseInt(columnValue[10].split("\\.")[0]) * (int) ModbusConstants.SCALE_FACTOR_BASE / Integer.parseInt(columnValue[8].split("\\.")[0]); }
                    if (columnValue[2].equalsIgnoreCase("QWORD")) { customDataList.modbusDataType = ModbusDataType.QWORD; }
                    else if (columnValue[2].equalsIgnoreCase("DWORD")) { customDataList.modbusDataType = ModbusDataType.DWORD; }
                    else if (columnValue[2].equalsIgnoreCase("LONG")) { customDataList.modbusDataType = ModbusDataType.LONG; }
                    else if (columnValue[2].equalsIgnoreCase("FLOAT")) { customDataList.modbusDataType = ModbusDataType.FLOAT; }
                    else if (columnValue[2].equalsIgnoreCase("WORD")) {customDataList.modbusDataType = ModbusDataType.WORD; }
                    else if (columnValue[2].equalsIgnoreCase("SHORT")) { customDataList.modbusDataType = ModbusDataType.SHORT; }
                    else if (columnValue[2].equalsIgnoreCase("BOOLEAN")) { customDataList.modbusDataType = ModbusDataType.BOOLEAN; }
                    else { throw new StageException(Errors.ERROR_007); }
                    resultList.add(customDataList);
                }
            }
        }
        catch (IOException e) { throw new StageException(Errors.ERROR_003); }
        AddressListGenerator addressListGenerator = new AddressListGenerator(resultList);
        addressListMenuList = addressListGenerator.getAddressListMenuList();
        return resultList;
    }
    @Override
    public List <AddressListMenu> getAddressList(){ return addressListMenuList; }
    private int getAddressInteger(String curAddress){
        int resultInt;
        if(curAddress.contains(".")){
            String [] columnBoolValue = curAddress.split("\\.");
            if(!columnBoolValue[1].equals("0")){ resultInt = -1; }
            else {resultInt = Integer.parseInt(columnBoolValue[0]);}
        }
        else { resultInt = Integer.parseInt(curAddress); }
        return resultInt;
    }
}
