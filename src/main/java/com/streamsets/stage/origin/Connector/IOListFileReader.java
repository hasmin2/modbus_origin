package com.streamsets.stage.origin.Connector;

import com.streamsets.pipeline.api.StageException;
import com.streamsets.stage.lib.Menus.CustomDataListMenu;
import com.streamsets.stage.origin.util.AddressListMenu;

import java.util.List;

public interface IOListFileReader {
    void init(String fileName) throws StageException;
    List<CustomDataListMenu> fileRead() throws StageException;
    List <AddressListMenu> getAddressList();
}
