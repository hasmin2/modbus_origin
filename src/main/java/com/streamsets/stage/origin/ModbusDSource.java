package com.streamsets.stage.origin;

import com.streamsets.pipeline.api.*;
import com.streamsets.stage.lib.Groups;
import com.streamsets.stage.lib.Menus.*;
import com.streamsets.stage.lib.ModbusConstants;

import java.util.List;

@StageDef(
        version = 1,
        label = ModbusConstants.STAGE_LABEL,
        description = ModbusConstants.STAGE_DESC,
        icon = ModbusConstants.STAGE_ICON,
        execution = ExecutionMode.STANDALONE,
        recordsByRef = true,
        onlineHelpRefUrl = ModbusConstants.STAGE_HELP_URL
)
@ConfigGroups(value = Groups.class)

@GenerateResourceBundle
public class ModbusDSource extends ModbusSource {
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.MODEL,
            defaultValue = ModbusConstants.ETHERNET_UDP_TYPE,
            label = ModbusConstants.UDP_FLAG_LABEL,
            description = ModbusConstants.UDP_FLAG_DESC,
            triggeredByValue = ModbusConstants.NETTYPE_ETH_OPTION,
            displayPosition = 10,
            group = ModbusConstants.COMM_GROUP
    )
    @ValueChooserModel(NetworkTypeChooserValues.class)
    public NetworkType isUdp;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.BOOLEAN,
            defaultValue = ModbusConstants.ADDRESS_OFFSET_VALUE,
            label = ModbusConstants.ADDRESS_OFFSET_LABEL,
            description = ModbusConstants.ADDRESS_OFFSET_DESC,
            dependsOn = "customDataListFromCsv",
            triggeredByValue = {ModbusConstants.KEPWARE_CSV_OPTION},
            displayPosition = 10,
            group = ModbusConstants.ADDRESS_GROUP
    )
    public boolean isBeginWithOne;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.BOOLEAN,
            defaultValue = ModbusConstants.TRANSFER_TYPE_VALUE,
            label = ModbusConstants.TRANSFER_MODE_LABEL,
            description = ModbusConstants.TRANSFER_MODE_DESC,
            displayPosition = 10,
            group = ModbusConstants.COMM_GROUP
    )
    public boolean transferMode;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.MODEL,
            defaultValue = "INTEGERTYPE",
            label = ModbusConstants.BOOLEAN_OUTPUT_LABEL,
            displayPosition = 10,
            group = ModbusConstants.COMM_GROUP,
            description = ModbusConstants.BOOLEAN_OUTPUT_DESC
    )
    @ValueChooserModel(BooleanOutPutTypeChooserValues.class)
    public BooleanOutPutType booleanOutputType;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.BOOLEAN,
            defaultValue = ModbusConstants.IS_LITTLE_ENDIAN_VALUE,
            label = ModbusConstants.IS_LITTLE_ENDIAN_LABEL,
            description = ModbusConstants.IS_LITTLE_ENDIAN_DESC,
            displayPosition = 10,
            group = ModbusConstants.COMM_GROUP
    )
    public boolean isLittleEndian;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.MODEL,
            defaultValue = ModbusConstants.LIST_FROM_UI_OPTION,
            label = ModbusConstants.DATA_MANAGE_SELECTION_LABEL,
            description = ModbusConstants.DATA_MANAGE_SELECTION_DESC,
            displayPosition = 10,
            group = ModbusConstants.ADDRESS_GROUP
    )
    @ValueChooserModel(ReadFileTypeChooserValues.class)
    public ReadFileType customDataListFromCsv;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.NUMBER,
            defaultValue = ModbusConstants.SCANRATE_VALUE,
            label = ModbusConstants.SCANRATE_LABEL,
            description = ModbusConstants.SCANRATE_DESC,
            min=0,
            max=86400000,
            displayPosition = 10,
            group = ModbusConstants.COMM_GROUP
    )
    public int scanRate;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.NUMBER,
            defaultValue = ModbusConstants.TIMEOUT_VALUE,
            label = ModbusConstants.TIMEOUT_LABEL,
            description = ModbusConstants.TIMEOUT_DESC,
            min=0,
            max=600000,
            displayPosition = 10,
            group = ModbusConstants.COMM_GROUP
    )
    public int timeout;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.STRING,
            defaultValue = ModbusConstants.IP_VALUE,
            label = ModbusConstants.IP_ADDR_LABEL,
            triggeredByValue = ModbusConstants.NETTYPE_ETH_OPTION,
            displayPosition = 10,
            group = ModbusConstants.COMM_GROUP,
            description = ModbusConstants.IP_ADDR_DESC
    )
    public String ipAddress;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.NUMBER,
            defaultValue = ModbusConstants.PORT_VALUE,
            label = ModbusConstants.PORT_LABEL,
            triggeredByValue = ModbusConstants.NETTYPE_ETH_OPTION,
            displayPosition = 10,
            group = ModbusConstants.COMM_GROUP,
            description = ModbusConstants.PORT_DESC
    )
    public int port;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.NUMBER,
            defaultValue = "0",
            label = ModbusConstants.COM_ID_LABEL,
            description = ModbusConstants.COM_ID_DESC,
            min=0,
            max=255,
            displayPosition = 10,
            group = ModbusConstants.COMM_GROUP
    )
    public int slaveId;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.STRING,
            defaultValue = ModbusConstants.FILEPATH_VALUE,
            label = ModbusConstants.FILEPATH_LABEL,
            dependsOn = "customDataListFromCsv",
            triggeredByValue = {ModbusConstants.KEPWARE_CSV_OPTION},
            displayPosition = 10,
            group = ModbusConstants.ADDRESS_GROUP,
            description = ModbusConstants.FILEPATH_DESC
    )
    public String filePathName;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.MODEL,
            label = "",
            description = "",
            dependsOn = "customDataListFromCsv",
            triggeredByValue = ModbusConstants.LIST_FROM_UI_OPTION,
            displayPosition = 210,
            group = ModbusConstants.ADDRESS_GROUP
    )
    @ListBeanModel
    public List<CustomDataListMenu> customDataListMenus;

    /** {@inheritDoc} */
    @Override
    public String getIPAddress() {return ipAddress;}
    @Override
    public int getPort() {return port;}
    @Override
    public String getFilePathName() {return filePathName;}
    @Override
    public int getTimeout() {return timeout;}
    @Override
    public int getScanRate() {return scanRate;}
    @Override
    public boolean getTransferMode() {return transferMode;}
    @Override
    public boolean getIsLittleEndian() {return isLittleEndian;}
    @Override
    public boolean getIsUdp() {
        boolean result;
        result = isUdp.name().equals(ModbusConstants.ETHERNET_UDP_TYPE);
        return result;
    }
    @Override
    public ReadFileType getCustomDataListFromCsv() {return customDataListFromCsv;}
    @Override
    public boolean getIsBeginWithOne() {return isBeginWithOne;}
    @Override
    public BooleanOutPutType getBooleanOutputType() {return booleanOutputType;}
    @Override
    public int getSlaveId() {return slaveId;}
    @Override
    public List<CustomDataListMenu> getCustomDataListMenus() {return customDataListMenus;}

}
