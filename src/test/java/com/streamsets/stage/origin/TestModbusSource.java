/**
 * Copyright 2015 StreamSets Inc.
 * <p>
 * Licensed under the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.streamsets.stage.origin;
import com.streamsets.pipeline.api.Record;
import com.streamsets.pipeline.sdk.SourceRunner;
import com.streamsets.pipeline.sdk.StageRunner;
import com.streamsets.stage.lib.Menus.*;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class TestModbusSource {
    private static final int MAX_BATCH_SIZE = 5;
    private static final Logger log = LoggerFactory.getLogger(TestModbusSource.class);

    @Test
    public void testOrigin() throws Exception {
        List<CustomDataListMenu> customDataListMenuList = new ArrayList<>();
        CustomDataListMenu customDataListMenu = new CustomDataListMenu();
        customDataListMenu.address= 4;
        customDataListMenu.modbusDataType= ModbusDataType.WORD;
        customDataListMenu.scale=10000;
        customDataListMenu.registerType= RegisterType.HOLDINGREGISTER;
        customDataListMenuList.add(customDataListMenu);
        customDataListMenu = new CustomDataListMenu();
        customDataListMenu.address= 1;
        customDataListMenu.modbusDataType= ModbusDataType.QWORD;
        customDataListMenu.scale=10000;
        customDataListMenu.registerType= RegisterType.HOLDINGREGISTER;
        customDataListMenuList.add(customDataListMenu);

        SourceRunner runner = new SourceRunner.Builder(ModbusDSource.class)
                .addConfiguration("transferMode", false)
                .addConfiguration("booleanOutputType", BooleanOutPutType.INTEGERTYPE)
                .addConfiguration("timeout", 5000)
                .addConfiguration("isLittleEndian", false)
                .addConfiguration("isBeginWithOne", false)
                .addConfiguration("customDataListFromCsv", ReadFileType.LISTFROMUI)
                .addConfiguration("scanRate", 1000)
                .addConfiguration("filePathName", "/data/home/hhiroot/Desktop/test.csv")
                .addConfiguration("ipAddress","localhost")
                .addConfiguration("port",502)
                .addConfiguration("slaveId",0)
                .addConfiguration("isUdp",NetworkType.UDP)
                .addConfiguration("customDataListMenus",customDataListMenuList)
                .addOutputLane("lane")
                .build();

        try {
            runner.runInit();
            final String lastSourceOffset = null;
            StageRunner.Output output = runner.runProduce(lastSourceOffset, MAX_BATCH_SIZE);
            //Assert.assertEquals("10", output.getNewOffset());
            List<Record> records = output.getRecords().get("lane");
            for (Record item : records){
                System.out.println(item.toString());
            }
            //Assert.assertEquals(10, records.size());
            //Assert.assertTrue(records.get(0).has("/source"));
            //Assert.assertEquals("Some Value", records.get(0).get("/fieldName").getValueAsString());

        } finally {
            runner.runDestroy();
        }
    }

}
