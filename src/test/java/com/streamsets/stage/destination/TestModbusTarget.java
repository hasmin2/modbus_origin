/**
 * Copyright 2015 StreamSets Inc.
 * <p>
 * Licensed under the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.streamsets.stage.destination;

import com.streamsets.pipeline.api.Field;
import com.streamsets.pipeline.api.Record;
import com.streamsets.pipeline.sdk.RecordCreator;
import com.streamsets.pipeline.sdk.TargetRunner;
import com.streamsets.stage.lib.Menus.NetworkType;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class TestModbusTarget {
    private static final Logger log = LoggerFactory.getLogger(TestModbusTarget.class);

    @Test
    public void testWriteSingleRecord() throws Exception {

        TargetRunner runner = new TargetRunner.Builder(ModbusTarget.class)
                .addConfiguration("timeout", 1000)
                .addConfiguration("isLittleEndian", false)
                .addConfiguration("isBeginWithOne", false)
                .addConfiguration("scanRate", 1000)
                .addConfiguration("ipAddress","localhost")
                .addConfiguration("port",502)
                .addConfiguration("slaveId",0)
                .addConfiguration("isUdp",NetworkType.TCPIP)
                .build();

        try {
            runner.runInit();
            Record record = RecordCreator.create();
            Map<String, Field> fields = new HashMap<>();
            fields.put("ipAddress", Field.create("10.7.10.181"));
            fields.put("address", Field.create("D6909"));
            fields.put("value", Field.create(1));
            fields.put("dataType", Field.create("SIGNED_INTEGER"));
            record.set(Field.create(fields));
            runner.runWrite(Arrays.asList(record));
            runner.runDestroy();

        } finally {
            runner.runDestroy();
        }
    }

}
