# Modbus_origin

This code is for "reading" from modbus TCP/UDP and serial.
Supports each data output's data format, little/big endian reading.
 
It is based on **Streamsets 3.8**



## Getting Started

This source code is based on Streamsets 3.8, Therefore you need to know basis of streamsets and custom origin's concept.

you can find it from https://github.com/streamsets/tutorials/blob/master/tutorial-origin/readme.md



### Prerequisites

I used latest version of IntelliJ and oracle Jdk 8-162. OpenJdk compatibility has not been tested. But should be no problem.

```
Intellij, and JDK 8 or later
```

### Installing

following command should be used to build "modbus_origin-1.0.0.tar.gz".

The "tar.gz" file should be copied into "${Streamsets_root}/user-lib" and decompressed.

```
mvn package -DskipTests 
....
cp [streamsets_root]/etc/user/lib/online_origin[version].tar.gz
tar -xvf online_origin[version].tar.gz
```



Also special permision should be required to get TCP connection and Command console.

open the  "${Streamsets_root}/etc/sdc-security.properties" 

and put line as below;

```
grant codebase "file://${sdc.dist.dir}/user-libs/modbus_origin/-" {
  permission java.security.AllPermission;
};
```

You Should restart the Streamsets. if you paste files while streamsets is running.

### Running in Streamsets
Once you open the streamsets pipeline, you can see below image like,

![modbus_Communication](./image/modbus_Communication.png)
 1. Device Name : set device name as you desired. 
    * note that device Name uses "filename".csv thus the csv filename should match its device name.
 2. Communication Type
    1) Ethernet(TCPIP/UDP) : Modbus uses ethernet connection it use 'IP' and 'port' to connect.
    2) Serial(RS232/485) : Modbus Uses RS232/RS485 Connection, it uses serial string such '/dev/ttyUSB0', implemented only, not tested so far.
 3. UDP connection (Ethernet option only) : check if modbus uses UDP  
 4. Skip duplicate value : if checked, skip value compare last values. reduce data amount with duplicated data.
 5. Boolean output type
    1) Converted to Integer value (1/0) output : True/false value will changed into 1/0 integer.
    2) Original (true/false) output
 6. scan rate (Millises) : Select a Communication rate, note that set to '0' for send as soon as can
 7. Timeout (Millisecs) : Timeout for each frame
 8 Modbus slave ID (Serial option only) : Input modbus slave ID
 9.IP Address (Ethernet option only) : input IP address.
 10-1. Modbus Port (Ethernet option only) : select modbus port. default is 502
 10-2. Serial Port String (Serial option only): input serial port path for connecting.
    
 
![modbus_Address](./image/modbus_Address.png)
1. Zero start address : if checked address will be display 0 not 1.
2. read from csv file 
    1) Read KEPWARE from home directory for address configure. not from UI. Note that the csv format is export from KepServer.
    2) Input my list using UI web interface directly.
    
3. File Path and Name : Input file path for the file. the filename should include extensions with case sensitive. ex)'/home/myHome/exam.csv'. File path can be retrieved system administrator 
    
3. register address list
   1) register type : select register type as per modbus protocol
   2) address : input register beginning address.
   3) data type : select data type for each address.
   4) scale : input scale integer as per permil. (1000 is 1.0X)                        
## Acknowledgments

- Hat tip to anyone who's code was used
- Inspiration
- etc